# language: es



  Característica: Validar detalle de la info de un producto

    Yo como usuario del portal de compras web
    Quiero verificar el detalle del producto
    Para tomar decisiones de compra

@Producto_SuccessFul
  Esquema del escenario: Validar detalle de productos

    Dado que me encuentro en el portal web de la pagina demozable
    Cuando ingreso a la seccion de laptops
      |opcionMenu|
      |<opcionMenu>|
    Entonces valido el detalle de la informacion del producto
    Ejemplos:
      |opcionMenu|
      |Laptops|