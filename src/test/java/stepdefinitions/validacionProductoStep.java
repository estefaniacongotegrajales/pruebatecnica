package stepdefinitions;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import questions.InternaproductoQuestions;
import tasks.ProductoTask;

public class validacionProductoStep {
    @Before
    public void configuracionInicial(){
        setTheStage(new OnlineCast());
    }

public void  setUp(){
        OnStage.setTheStage(new OnlineCast());
    }
    @Dado("^que me encuentro en el portal web de la pagina demozable$")
    public void que_me_encuentro_en_el_portal_web_de_la_pagina_demozable() {
        theActorCalled("Usuario").wasAbleTo(Open.url("https://www.demoblaze.com/index.html"));

    }

    @Cuando("^ingreso a la seccion de laptops$")
    public void ingreso_a_la_seccion_de_laptops(DataTable arg1) {
        theActorCalled("Usuario").attemptsTo(ProductoTask.clicklaptops());
    }

    @Entonces("^valido el detalle de la informacion del producto$")
    public void valido_el_detalle_de_la_informacion_del_producto() {
        theActorInTheSpotlight().should(seeThat(InternaproductoQuestions.thisElementPresentProduct()));

    }


}
