package runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/validarprecioproducto.feature",
        tags = "@Producto_SuccessFul",
        glue = "stepdefinitions"

)
public class validacionProductoRunner {

}
