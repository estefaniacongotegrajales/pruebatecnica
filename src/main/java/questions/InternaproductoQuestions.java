package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static userinterface.InternaProductoUI.*;

public class InternaproductoQuestions implements Question {

    @Override
    public Object answeredBy(Actor actor) {

    return  CLICK_NOMBRE_PRODUCTO.resolveFor(actor).isPresent() &&
            CLICK_PRECIO_PRODUCTO.resolveFor(actor).isPresent() &&
            CLICK_DESCRIPCION_PRODUCTO.resolveFor(actor).isPresent() &&
            CLICK_IMAGEN_PRODUCTO.resolveFor(actor).isPresent();

    }

    public static InternaproductoQuestions thisElementPresentProduct (){
    return  new InternaproductoQuestions();

    }
}
