package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import userinterface.ProductoUI;

public class ProductoTask implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {
     actor.attemptsTo(Click.on(ProductoUI.CLICK_LAPTOPS));
     actor.attemptsTo(Click.on(ProductoUI.INTERNA_LAPTOPS));
    }
     public static Performable clicklaptops() {
         return Tasks.instrumented(ProductoTask.class);
        }
    }

