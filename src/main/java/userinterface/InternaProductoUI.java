package userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class InternaProductoUI {

    public static final Target CLICK_NOMBRE_PRODUCTO= Target.the("Nombre del producto").located(By.xpath("//*[@id=\"tbodyid\"]/h2"));
    public static final Target CLICK_PRECIO_PRODUCTO= Target.the ("Precio  producto").located(By.xpath("//*[@id=\"tbodyid\"]/h3"));
    public static final Target CLICK_DESCRIPCION_PRODUCTO= Target.the ("Descripcion del producto").located(By.xpath("//*[@id=\"more-information\"]"));
    public static final Target CLICK_IMAGEN_PRODUCTO= Target.the ("Imagen del producto").located(By.xpath("//*[@id=\"imgp\"]/div/img"));

}
